# QWOP Key Proxy

This repository contains code that will proxy the key presses of a microcontroller's
output from serial to keyboard mappings with correct timings.

## Installation

    pip install -r requirements.txt

## Usage

    ./proxy_key.py
