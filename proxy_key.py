#!/usr/bin/env python
import time
from threading import Thread
from Queue import Queue

import serial
import pykeyboard


keyboard = pykeyboard.PyKeyboard()
ser = serial.Serial('/dev/tty.usbmodemfa1342', 9600)


# Keys are tokens, values are Queues
queues = {}


def create_handler(token):
    """Handler for each character queue.

    Method will release the key for each character at the appropriate timing.
    """
    q = queues[token]
    def handler():
        while True:
            token = q.get()
            # Allow the event to register with Flash
            time.sleep(0.08)
            # release the key
            keyboard.release_key(token)
    return handler


def loop():
    """Primary loop for reading serial data."""
    while True:
        token = ser.read()
        # press the key
        keyboard.press_key(token)
        # Queue up key for release key at a later time
        queues[token].put(token)
        time.sleep(0.1)


def setupQueues():
    """Helper function to create the Queues we care about."""
    for ch in ['q', 'w', 'o', 'p']:
        queues[ch] = Queue()


def setupThreads():
    """Setup the threads."""
    for key in queues.keys():
        t = Thread(target=create_handler(key))
        t.daemon = True
        t.start()


if __name__ == '__main__':
    setupQueues()
    setupThreads()
    try:
        loop()
    except KeyboardInterrupt as e:
        print '\nExiting!\n\n'
